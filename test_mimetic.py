from unittest import TestCase


class TestMimetic(TestCase):
    def test_row_contains_seq(self):
        from mimetic import row_contains_seq
        row = {'w1': 'C', 'w2': 'D', 'w3': 'G', 'v': 'Y4'}
        assert (row_contains_seq(row, 'CDG'))
        row = {'w1': 'C', 'w2': 'D', 'w3': 'G', 'v': 'Y4'}
        assert (not row_contains_seq(row, 'CDF'))

    def test_query_seq(self):
        from mimetic import query_seq
        matches = query_seq('C')
        assert (len(matches) == 3)

        matches = query_seq('CD')
        assert (len(matches) == 2)

        matches = query_seq('CDF')
        assert (len(matches) == 1)

        matches = query_seq('B')
        assert (len(matches) == 2)

        matches = query_seq('BC')
        assert (len(matches) == 1)

        matches = query_seq('')
        assert (len(matches) == 0)

        matches = query_seq('TXAD')
        assert (len(matches) == 0)

    def test_seq_to_tuple(self):
        from mimetic import seq_to_tuple
        row = {'w1': 'C', 'w2': 'D', 'w3': 'G', 'v': 'Y4'}
        assert (seq_to_tuple(row)==('CDG','Y4'))
        row = {'w1': 'C', 'w2': 'D', 'v': 'Y4'}
        assert (seq_to_tuple(row)==('CD','Y4'))


class TestTrieNode(TestCase):
    def setUp(self):
        from trie_node import TrieLeaf
        from trie_node import TrieRoot
        from trie_node import TrieNode
        self.seq_trie = TrieRoot()
        self.seq_trie.add_child(
            TrieNode('A').
                add_child(TrieLeaf('X1')).
                add_child(
                    TrieNode('B').
                    add_child(TrieLeaf('Y1'))
              )
        )

        self.seq_trie.add_child(
            TrieNode('B').
                add_child(TrieLeaf('X2')).
                add_child(TrieNode('C').add_child(TrieNode('D')
                                                  .add_child(TrieLeaf('Y2'))
                                                  )
                        )
        )

    def test_get_value_for_sequence(self):
        sequence,is_partial_match = self.seq_trie.get_value_for_sequence('AB')
        assert(sequence == 'Y1')

        sequence,is_partial_match = self.seq_trie.get_value_for_sequence('BCD')
        assert(sequence == 'Y2')

        sequence,is_partial_match = self.seq_trie.get_value_for_sequence('A')
        assert(sequence == 'X1')

        sequence,is_partial_match = self.seq_trie.get_value_for_sequence('B')
        assert(sequence == 'X2')

        sequence,is_partial_match = self.seq_trie.get_value_for_sequence('G')
        assert(sequence is None)

        sequence,is_partial_match = self.seq_trie.get_value_for_sequence('ABC')
        assert(sequence is None)

        sequence,is_partial_match = self.seq_trie.get_value_for_sequence('ABCDE')
        assert(sequence is None)

    def test_add_seq_to_trie(self):
        from trie_node import TrieRoot
        root = TrieRoot()
        root.add_seq_to_trie('X1','A')
        root.add_seq_to_trie('Y1','AB')
        root.add_seq_to_trie('X2','B')
        root.add_seq_to_trie('Y2','BCD')
        assert (root == self.seq_trie)

    def test_transform_seq(self):
        from mimetic import transform_sequence
        sequence = transform_sequence('ABCDEF')
        assert(sequence ==  [('AB', 'Y1'), ('C', 'X3'), ('D', 'X4'), ('E', 'X5'), ('F', None)])


