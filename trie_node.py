class TrieNode(object):
    def __init__(self, character=None, value=None):
        self.children = []
        self.character = character
        self.value = value

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not self.__eq__(other)

    def add_child(self, node):
        self.children.append(node)
        return self

    def get_children(self):
        return self.children

    def get_leaf(self):
        for child in self.children:
            if child.is_leaf():
                return child

    def get_matching_children(self, character):
        matching_nodes = []
        for child in self.get_children():
            if child.get_character() == character:
                matching_nodes.append(child)
        return matching_nodes

    def get_value_for_sequence(self, seq):
        matching_children = []
        is_partial_match = False
        if len(seq) > 0:
            matching_children = self.get_matching_children(seq[0])
        num_matching_children = len(matching_children)
        if (num_matching_children == 0 and len(seq)== 0):
            # check if the current node has any leaf attached to it
            leaf = self.get_leaf()
            num_children = len(self.get_children())
            if num_children > 1:
                is_partial_match = True
            if leaf:
                value = leaf.get_value()
                return value,is_partial_match
            else:
                return None,is_partial_match
        elif (num_matching_children == 0):
            return None,is_partial_match
        else:
            # partial match, proceed with looking the match for next character
            for child in matching_children:
                return child.get_value_for_sequence(seq[1:])

    def is_root(self):
        return False

    def is_leaf(self):
        return False

    def get_value(self):
        if self.is_leaf():
            return self.value
        else:
            return None

    def get_character(self):
        if (~ self.is_leaf()):
            return self.character
        else:
            return None

    def add_seq_to_trie(self,value,seq):
        if not seq:
            self.add_child(TrieLeaf(value))
        else:
            prefix = seq[0]
            matching_children = self.get_matching_children(prefix)
            matching_child = None
            if (len(matching_children)==0):
                matching_child = TrieNode(prefix)
                self.add_child(matching_child)
            else:
                matching_child = matching_children[0]
            matching_child.add_seq_to_trie(value,seq[1:])

class TrieRoot(TrieNode):
    def is_root(self):
        return True



class TrieLeaf(TrieNode):

    def __init__(self, value=None):
        super().__init__(value=value)

    def is_leaf(self):
        return True