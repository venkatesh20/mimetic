# input = [A, B, C, D, E, F...]
#
# dict / db
#
# id  w1,  w2,  w3,  w4,   v
# 1    A                   X1
# 2    B                   X2
# 3    A    B              Y1
# 4    C                   X3
# 5    D                   X4
# 6    B    C    D         Y2
# 7    C    D    F         Y3
# 8    C    D    G         Y4
# 9    E                   X5
#
# output = [ Y1, X3, X4, X5, .. ]
#
# ORM format
# db.where ( { w1 => A } ) ==> (SELECT * FROM DB_TABLE WHERE w1 == A)
# ouput ===> [ { w1 => A }, { w1 => A, w2 => B } ]
#
# db.where ( { w1 => A, w2 => B } ) ==> SELECT * FROM DB_TABLE WHERE w1 == A AND w2 == B
# output ===> [ { w1 => A, w2 => B } ]
#
# db.where ( { w1 => A, w2 => B, w3 => C } )
# output ===> []
from trie_node import TrieRoot

seq_data = []
seq_data.append({'w1': 'A', 'v': 'X1'})
seq_data.append({'w1': 'B', 'v': 'X2'})
seq_data.append({'w1': 'A', 'w2': 'B', 'v': 'Y1'})
seq_data.append({'w1': 'C', 'v': 'X3'})
seq_data.append({'w1': 'D', 'v': 'X4'})
seq_data.append({'w1': 'B', 'w2': 'C', 'w3': 'D', 'v': 'Y1'})
seq_data.append({'w1': 'C', 'w2': 'D', 'w3': 'F', 'v': 'Y3'})
seq_data.append({'w1': 'C', 'w2': 'D', 'w3': 'G', 'v': 'Y4'})
seq_data.append({'w1': 'E', 'v': 'X5'})


def row_contains_seq(row, seq):
    """

    :param row: The row dict containing the sequence information
    :param seq: The sequence string to be queried
    :return: boolean
    """

    is_seq_present = True

    # return none in case of empty string
    if seq is None or (len(seq) == 0):
        is_seq_present = False

    for i in range(len(seq)):
        if row.get('w' + str(i + 1)) != seq[i]:
            is_seq_present = False
            break

    return is_seq_present


def query_seq(seq):
    """

    :param seq: string sequence to be queried
    :return: matched rows from the seq data
    """
    matches = []
    for row in seq_data:
        if row_contains_seq(row, seq):
            matches.append(row)
    return matches

def seq_to_tuple(row):
    value = None
    seq = ''
    for k in sorted(row.keys()):
        if k=='v':
            value = row[k]
        else:
            seq += row[k]
    return (seq,value)


seq_trie = TrieRoot()

def build_trie_for_prefix(character):
    matching_children = seq_trie.get_matching_children(character)
    if (len(matching_children)==0):
        seq_data_matches = query_seq(character)
        for row in seq_data_matches:
            seq,value = seq_to_tuple(row)
            seq_trie.add_seq_to_trie(value,seq)


def transform_sequence(seq,outputs=None):
    """
    Main function to transform the input sequence in to output
    :param seq: string sequence
    :return: output values
    """
    if (outputs==None):
        outputs = []
    build_trie_for_prefix(seq[0])
    last_successful_result = None
    for i in range(1,len(seq)+1):
        current_prefix = seq[:i]
        seq_value,is_partial_match = seq_trie.get_value_for_sequence(current_prefix)
        if (seq_value):
            last_successful_result = (current_prefix,seq_value)
            if (i==len(seq)):
                outputs.append(last_successful_result)
        else:
            if (last_successful_result is None):
                outputs.append((current_prefix,None))
            else:
                outputs.append(last_successful_result)
                transform_sequence(seq[(i-1):],outputs)
            break

    return outputs

